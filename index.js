const express = require("express");

const mongoose = require("mongoose");
const cors = require ("cors");

const userRoute = require("./routes/userRoute");

const productRoute = require("./routes/productRoute");


// Server
const app = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


//Database connection
mongoose.connect("mongodb+srv://mervin03091996:admin123@zuitt-bootcamp.cljzexh.mongodb.net/productBookingAPI?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection.once("open",() => console.log(`Now connected to cloud database!`));

 app.use("/users", userRoute);

 app.use("/products", productRoute);


app.listen(process.env.Port || 5000,()=> console.log (`Now 
	connected to port ${process.env.Port ||5000}`));
