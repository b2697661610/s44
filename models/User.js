const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	mobileNumber: {
		type : String,
		required : [true, "mobileNumber is required"]
	},

	isAdmin : {
		type : Boolean,
		default : false
	},
	street: {
		type : String,
		required : ["default"]
	},
	city: {
		type : String,
		required : ["default"]
	},
	country : {
		type : String,
		required : ["default"]
	},
	orders : [
		{
			products : {
				productsName : String,
				quantity: Number,
			},
			totalAmount: Number,

			purchaseOn : Date
		}
	]
});


module.exports = mongoose.model("User", userSchema);